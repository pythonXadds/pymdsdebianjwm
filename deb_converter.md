
#How to convert a .deb file and make it into a .rpm file instead.



**To convert a .deb file one needs to download**

 `sudo yum install alien` *fedora*

Then in order to use the application one needs to navigate to the file location ie; .deb , then type this into the command line:

|sudo alien *your package name.dev*|

This should result in the deb package being converted to a .rpm or visa versa. 

it's also important to note that if you add a ' -i ' to the copmmand, it alien will install the application as well!


to install this on a debian based distro, you coan use the command:

|sudo apt-get install alien|